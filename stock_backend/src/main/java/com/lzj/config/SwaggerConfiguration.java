//package com.lzj.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Import;
//
///**
// * @author lizhijun
// * @Data 2024/3/25
// * @description:定义swagger配置类
// */
//@Configuration
//@EnableSwagger2
////@EnableKnife4j
////@Import(BeanValidatorPluginsConfiguration.class)    //导入其它配置类，让配置生效
//public class SwaggerConfiguration {
//    @Bean
//    public Docket buildDocket() {
//        //构建在线API概要对象
//        return new Docket(DocumentationType.SWAGGER_2)
//                .apiInfo(buildApiInfo())
//                .select()
//                // 要扫描的API(Controller)基础包
//                .apis(RequestHandlerSelectors.basePackage("com.lzj.stock.controller"))
//                .paths(PathSelectors.any())
//                .build();
//    }
//    private ApiInfo buildApiInfo() {
//        //网站联系方式
//        Contact contact = new Contact("future","https://www.baidu.com/","3254166356@qq.com");
//        return new ApiInfoBuilder()
//                .title("今日指数-在线接口API文档")//文档标题
//                .description("这是一个方便前后端开发人员快速了解开发接口需求的在线接口API文档")//文档描述信息
//                .contact(contact)//站点联系人相关信息
//                .version("1.0.0")//文档版本
//                .build();
//    }
//}