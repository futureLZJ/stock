package com.lzj.controller;

import com.lzj.pojo.entity.SysUser;
import com.lzj.service.UserService;
import com.lzj.vo.req.LoginReqVo;
import com.lzj.vo.resp.LoginRespVo;
import com.lzj.vo.resp.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api")
//@Api("用户认证相关接口")
public class UserController {


    @Autowired
    private UserService userService;


    //@ApiOperation(value="根据用户姓名查询信息",notes="用户信息查询",response=SysUser.class)
    //@ApiImplicitParam(paramType="path",name="userName",value="用户名",required = false)
    @RequestMapping("user/{userName}")
    public SysUser getUserByName(@PathVariable("UserName") String userName){
            return userService.getUserByName(userName);
    }
    //@ApiOperation(value="用户登录功能",notes="用户登录",response = Result.class)
    @PostMapping("/login")
    public Result<LoginRespVo> login(@RequestBody LoginReqVo loginReqVo) {
        Result<LoginRespVo> loginRespVo=userService.login(loginReqVo);
        return loginRespVo;

    }
    @GetMapping("/captcha")
    //@ApiOperation(value="获得验证码",notes="验证码",response=Result.class)
    public Result<Map> getCaptchaCode(){
        return userService.getCaptchaCode();
    }


}
